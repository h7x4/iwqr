# iwqr

> Share `iwd` networks easily using a QR code

### Usage

_The tool might require root permissions due to it reading from `/var/lib/iwd`_

**List all supported saved networks:**

`iwqr list`

**Show QR code for a specified network:**

`iwqr show <NETWORK_NAME>`

![](./assets/illustration.png)

#![feature(iter_array_chunks)]

use clap::{Parser, Subcommand};
use qrcode::QrCode;

fn hex_to_char(x: &[char; 2]) -> char {
    (x[0].to_digit(16).unwrap() * 16 + x[1].to_digit(16).unwrap()) as u8 as char
}

fn char_to_hex(x: char) -> [char; 2] {
    [
        char::from_digit((x as u32) >> 4, 16).unwrap(),
        char::from_digit((x as u32) & 15, 16).unwrap(),
    ]
}

fn list_psk_networks() {
    for x in glob::glob("/var/lib/iwd/*.psk").unwrap() {
        match x {
            Ok(x) => {
                let network_name = x.file_stem().unwrap().to_str().unwrap();

                let network_name = if network_name.chars().next().unwrap() == '=' {
                    network_name
                        .chars()
                        .skip(1)
                        .array_chunks::<2>()
                        .map(|x| hex_to_char(&x))
                        .collect::<String>()
                } else {
                    network_name.to_string()
                };

                println!("{network_name}");
            }
            Err(e) => match e.error().kind() {
                std::io::ErrorKind::PermissionDenied => {
                    eprintln!(
                        "Failed to access {:?}: Permission denied! (try running as root)",
                        e.path()
                    );
                    return;
                }
                _ => panic!("{e:?}"),
            },
        }
    }
}

fn print_qr(ssid: String) {
    let file_stem = if ssid
        .chars()
        .find(|&c| !c.is_alphanumeric() && !"-_ ".contains(c))
        .is_some()
    {
        "=".chars()
            .chain(
                ssid.chars()
                    .map(|x| char_to_hex(x).into_iter().collect::<String>())
                    .collect::<String>()
                    .chars(),
            )
            .collect::<String>()
    } else {
        ssid.clone()
    };

    let content = match std::fs::read_to_string(format!("/var/lib/iwd/{file_stem}.psk")) {
        Ok(data) => data,
        Err(e) => match e.kind() {
            std::io::ErrorKind::NotFound => {
                eprintln!("No saved network with SSID: {ssid}");
                return;
            }
            std::io::ErrorKind::PermissionDenied => {
                eprintln!("Failed to access /var/lib/iwd/{file_stem}.psk: Permission denied! (try running as root)");
                return;
            }
            _ => panic!("{e}"),
        },
    };

    let mut password = None;
    let mut hidden = false;

    for line in content.lines() {
        if line.starts_with("Passphrase=") {
            password = Some(line.strip_prefix("Passphrase=").unwrap());
            continue;
        }
        if line == "Hidden=true" {
            hidden = true;
            continue;
        }
    }

    if password.is_none() {
        eprintln!("No password saved for this network!");
        return;
    }

    let password = password.unwrap();

    let qr_code = QrCode::new(format!("WIFI:T:WPA;S:{ssid};P:{password};H:{hidden};;")).unwrap();
    println!(
        "{}",
        qr_code
            .render()
            .dark_color("\x1b[48;2;0;0;0m  \x1b[0;0m")
            .light_color("\x1b[48;2;255;255;255m  \x1b[0;0m")
            .build()
    );
}

#[derive(Debug, Subcommand)]
enum Command {
    /// List supported saved networks
    #[command()]
    List,
    /// Show a QR code for the specified network
    #[command()]
    Show {
        #[arg()]
        network_name: String,
    },
}

#[derive(Debug, Parser)]
#[command(author, version, about, long_about)]
struct Cli {
    #[command(subcommand)]
    command: Command,
}

fn main() {
    let args = Cli::parse();

    match args.command {
        Command::List => list_psk_networks(),
        Command::Show { network_name } => print_qr(network_name),
    }
}
